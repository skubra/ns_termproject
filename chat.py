import sys
sys.path.append('src')
from DBConnection import DBConnection
from flask import Flask, render_template, request, redirect
from flask_fontawesome import FontAwesome
from flask_login import LoginManager, current_user, login_user, logout_user, login_required, UserMixin
import json
import hashlib 

class User(UserMixin):
    logged_user = None
    def __init__(self, username):
        self.username = username
        self.id = None
        self.name = None

    def get_id(self):
        return self.username

    @staticmethod
    def get(username):
        user = User(username)
        if (users == None):
            return

        for u in users:
            if u[2] == username:
                user.id = u[0]
                user.name = u[1]
                User.logged_user = user


        return user

app = Flask(__name__)
fa = FontAwesome(app)

app.secret_key = 'NETWORK SECURITY AGU 2020'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

with open('config.json', 'r') as f:
    data = json.load(f)

db = DBConnection(data['connection_string'])
open_conversation = 0
users = None
conversations_cache = None

def get_user_password(user_name):
    for user in users:
        if user[2] == user_name:
            return user[3]

@login_manager.user_loader
def load_user(username):
    return User.get(username)

@app.route("/login", methods=['GET', 'POST'])
def login():
    global users
    users = db.get_users()

    if request.method == 'GET':
        return render_template('login.html')

    form_prm = request.form
    
    user_name = form_prm['user_name']
    user_name_b = user_name.encode()

    salt = hashlib.md5(user_name_b)
    hashed = hashlib.md5((salt.hexdigest().upper() + form_prm['password']).encode())
    hashed_value = hashed.hexdigest().upper()
    
    print (get_user_password(user_name), hashed_value)

    if (get_user_password(user_name) == hashed_value):
        user = User(user_name)
        login_user(user)

        return redirect('/')

    else :
        return render_template('login.html')

@app.route('/')
@login_required
def hello_world():
    conversations = db.get_conversations(User.logged_user.id)
    global conversations_cache
    conversations_cache = conversations
    contacts = db.get_contacts(User.logged_user.id)
    
    return render_template('index.html', conversations=conversations, contacts=contacts)

@app.route('/get_msgs')
@login_required
def get_messages():
    global open_conversation

    conversation_id = request.args.get('conv_id')
    if (conversation_id != None):
        open_conversation = conversation_id

    data = db.get_messages(open_conversation)
    data_str = ""
    for row in data:
        row_str = str(row[0]) + ";" + str(row[1]) + ";" + str(row[2]) + ";"  \
                + str(row[3]) + ";" + str(row[4]) + ";" + str(row[5]) + "\n"
        data_str += row_str 

    return data_str

@app.route('/send_msg')
@login_required
def send_a_message():
    new_message = request.args.get('msg')
    if (new_message == ""):
        return 404

    db.add_message(new_message, 1, User.logged_user.id, open_conversation)

    return new_message

@app.route('/add_contact')
@login_required
def add_contact():
    contact_uname = request.args.get('uname')
    db.add_contact(User.logged_user.id, contact_uname)

    return redirect('/')


@app.route('/add_conversation')
@login_required
def add_conversation():
    conv_title = request.args.get('title')
    usernames = request.args.get('unames') + User.logged_user.username
    username_arr = usernames.split(';')
   
    db.create_conversation(conv_title)
    db.add_people_to_conversation(username_arr)
    conversations = db.get_conversations(User.logged_user.id)
    contacts = db.get_contacts(User.logged_user.id)
    
    return render_template('index.html', conversations=conversations, contacts=contacts)

# @app.route('/get_conversations')
# @login_required
# def get_conversations():
#     data = db.get_conversations(User.logged_user.id)
#     if (data == None):
#         global conversations_cache
#         data = conversations_cache
   
#     data_str = ""
#     for row in data:
#         row_str = str(row[0]) + ";" + str(row[1]) + ";" + str(row[2]) + "\n"
#         data_str += row_str 

#     return data_str


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('login.html')

if __name__ == "__main__":
    app.run()