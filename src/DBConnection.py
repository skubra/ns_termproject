import pyodbc

class DBConnection:
	""" 
	Veritabanı bağlantısının sağlandığı ve işlemlerinin gerçekleştirildiği sınıf
	"""

	def __init__(self, connection_string):
		self.connection_string = connection_string
		self.connection = pyodbc.connect(self.connection_string)

	def get_conversations(self, user_id):
		"""  """
		try:
			cursor = self.connection.cursor()
			cursor.execute("[dbo].[sp_GET_CONVERSATIONS] ?", (user_id))
			res = cursor.fetchall()
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at get_conversations func: ', sqlstate)
			return None

		return res

	def get_messages(self, conversation_id):
		try:
			cursor = self.connection.cursor()
			cursor.execute("[dbo].[sp_GET_MESSAGES] ?", (conversation_id))
			res = cursor.fetchall()
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at get_messages func: ', sqlstate)
			return 'ERROR_MSGS'

		return res

	def add_message(self, msg, msg_type, user_id, conversation_id):
		try:
			cursor = self.connection.cursor()
			cursor.execute('[dbo].[sp_ADD_MESSAGE] ?, ?, ?, ?', (user_id, conversation_id, msg, msg_type))
			cursor.commit()
		
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at add_message func: ', sqlstate)
			print ()

	def get_users(self):
		try:
			cursor = self.connection.cursor()
			cursor.execute("[dbo].[sp_GET_USERS] ")
			res = cursor.fetchall()
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at get_users func: ', sqlstate)
			return 'ERROR_GETUSER'

		return res

	def get_contacts(self, user_id):
		try:
			cursor = self.connection.cursor()
			cursor.execute("[dbo].[sp_GET_CONTACTS] ?", (user_id))
			res = cursor.fetchall()
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at get_contacts func: ', sqlstate)
			return 'ERROR_CONTACTS'

		return res 

	def add_contact(self, user_id, contact_uname):
		try:
			cursor = self.connection.cursor()
			cursor.execute('[dbo].[sp_ADD_CONTACT] ?, ?', (user_id, contact_uname))
			cursor.commit()
		
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at add_contact func: ', sqlstate)
			print ()

	def create_conversation(self, title):
		try:
			cursor = self.connection.cursor()
			cursor.execute('[dbo].[sp_CREATE_CONVERSATION] ?', (title))
			cursor.commit()
		
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at create_conversation func: ', sqlstate)

	def add_people_to_conversation(self, users):
		try:
			for user in users:
				cursor = self.connection.cursor()
				cursor.execute('[dbo].[sp_ADD_PEOPLE_TO_CONVERSATION] ?', (user))
				cursor.commit()
		
		except pyodbc.Error as ex:
			sqlstate = ex.args[0]
			print ('Error at add_people_to_conversation func: ', sqlstate)
